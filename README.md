# infosec data



## Wild brute force

This will contain users and passwords used into the wild against my online honeypot.

File logins.txt will contain the combinations of user:password as received from the attackers.

Feel free to use them in your tests.

## Disclaimer

I'm not responsible of any misuse of this information, as it's being already used in bruteforce attacks that I'm receiving.

If you find your password listed here, change it immediatly.